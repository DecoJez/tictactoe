import sys

import numpy as np

from MatrixException import MatrixException


class Board(object):
    """
    Board of Tic Tac Toe game.
    Attributes:
        board (np.array): board
    """

    def __init__(self):
        """
        Initialization of Board.
        """
        self.board = None
        self.init_board()

    @staticmethod
    def handle_stdin():
        """
        Stdin from user.
        Returns:
            board (list): List of rows
        """
        rows = []
        for line in sys.stdin:
            if not line.strip():
                return rows
            else:
                rows.append(list(line.strip()))

    @staticmethod
    def check_board(board):
        """
        Validate the shape of board.
        Args:
            board (numpy.array): Board to verification

        """
        try:
            row, column = board.shape
        except ValueError:
            raise MatrixException('Board isn\' NxN')
        if row != column:
            raise MatrixException('Row number isn\'t equal to column number')

    def init_board(self):
        """
        Initialization and validation of board.

        """
        board = self.handle_stdin()
        np_board = np.asarray(board)
        self.check_board(np_board)
        self.board = np_board

    def get_board(self):
        """
        Getter for board.
        Returns:
            board (numpy.array): The returned value

        """
        return self.board
