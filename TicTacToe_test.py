import unittest
from unittest.mock import patch

import numpy as np
from ddt import ddt, data, unpack

from TicTacToe import TicTacToe


@ddt
class TicTacToeTest(unittest.TestCase):

    def setUp(self):
        board = np.asarray([['X', 'X'], ['X', 'X']])
        self.ticTacToe = TicTacToe(board)

    @patch('TicTacToe.exit')
    @data([[['X', 'X'], ['.', 'X']], 'X', 'X'], [[['X', '.'], ['Y', '.']], 'Y', False])
    @unpack
    def test_check_row(self, board, player, result, mockExit):
        self.ticTacToe.board = np.asarray(board)
        self.ticTacToe.check_rows(player)
        self.assertEqual(result, self.ticTacToe.winner)

    @patch('TicTacToe.exit')
    @data([[['X', 'X'], ['.', 'X']], 'X', 'X'], [[['X', '.'], ['Y', '.']], 'Y', False])
    @unpack
    def test_check_row(self, board, player, result, mockExit):
        self.ticTacToe.board = np.asarray(board)
        self.ticTacToe.check_rows(player)
        self.assertEqual(result, self.ticTacToe.winner)

    @patch('TicTacToe.exit')
    @data([[['X', 'X'], ['.', 'X']], 'X', 'X'], [[['X', '.'], ['Y', '.']], 'Y', False])
    @unpack
    def test_check_column(self, board, player, result, mockExit):
        self.ticTacToe.board = np.asarray(board)
        self.ticTacToe.check_columns(player)
        self.assertEqual(result, self.ticTacToe.winner)

    @patch('TicTacToe.exit')
    @data([[['X', '.'], ['.', 'X']], 'X', 'X'], [[['X', 'Y'], ['Y', '.']], 'Y', 'Y'], [[['X', '.'], ['Y', '.']], 'Y', False])
    @unpack
    def test_check_diagonal(self, board, player, result, mockExit):
        self.ticTacToe.board = np.asarray(board)
        self.ticTacToe.check_diagonal(player)
        self.assertEqual(result, self.ticTacToe.winner)

    @patch('TicTacToe.exit')
    @data([['X', 'X', 'X'], 'X', 'X'], [['X', 'X', 'O'], 'X', False], [['X', 'X', '.'], 'X', False])
    @unpack
    def test_search_for_winner(self, entity, player, result, mockExit):
        self.ticTacToe.search_for_winner(entity, player)
        self.assertEqual(result, self.ticTacToe.winner)

    @patch('TicTacToe.exit')
    @patch('TicTacToe.mutex')
    def test_mark_winner(self, mockMutex, mockExit):
        self.ticTacToe.mark_winner('X')
        self.assertEqual('X', self.ticTacToe.winner)

    @data([False, '.'], ['X', 'X'])
    @unpack
    def test_proclaim_winner(self, winner, result):
        self.ticTacToe.winner = winner
        self.assertEqual(result, self.ticTacToe.proclaim_winner())

if __name__ == "__main__":
    unittest.main()
