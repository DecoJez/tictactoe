import threading
import functools


def threaded(f):
    """
    Decorator to multithread operations.
    Args:
        f: function to wrap

    Returns:
        wrapped function
    """
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        thread = threading.Thread(target=f, args=args, kwargs=kwargs)
        thread.start()
        return thread
    return wrapper


def skip_if_winner_exist(mutex):
    """
    Decorator to check if winner exist. If yes, then terminate thread.
    Args:
        mutex (object): mutex for winner variable

    Returns:
        wrapped function
    """
    def _skip_if_winner_exist(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            mutex.acquire()
            if not args[0].winner:
                mutex.release()
                return f(*args, **kwargs)
            else:
                mutex.release()
                exit()
        return wrapper
    return _skip_if_winner_exist
