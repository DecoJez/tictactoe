import unittest
from unittest.mock import patch

import numpy as np

from Board import Board
from MatrixException import MatrixException


class BoardTest(unittest.TestCase):

    def test_check_board(self):
        array = [['X', 'X'], ['X', 'X']]
        np_board = np.asarray(array)
        Board.check_board(np_board)

    def test_check_board_fail(self):
        array = [['X', 'X', 'X'], ['X', 'X', 'X']]
        np_board = np.asarray(array)
        with self.assertRaises(MatrixException):
            Board.check_board(np_board)

    @patch('__main__.Board.init_board')
    def test_get_board(self, mockInitBoard):
        np_board = np.asarray([['X', 'X'], ['X', 'X']])
        board_instance = Board()
        board_instance.board = np_board
        np.testing.assert_array_equal(np_board, board_instance.get_board())


if __name__ == "__main__":
    unittest.main()
