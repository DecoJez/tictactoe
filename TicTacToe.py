import threading

from Board import Board
from decorators import threaded, skip_if_winner_exist

mutex = threading.Lock()


class TicTacToe(object):
    """
    Class for getting winner of Tic Tac Toe game.
    Attributes:
        board (np.array): current board
        winner (str): winner

    """

    def __init__(self, board):
        """
        Initialization of TicTacToe.
        Args:
            board (np.array): current board
        """
        self.board = board
        self.winner = False

    def check_win(self):
        """
        Search for win condition for all players.
        Returns:
            winner (str): The winner.
        """
        players = ['X', 'Y']
        for player in players:
            thread = self.check_player(player)
            thread.join()
        return self.proclaim_winner()

    @threaded
    def check_player(self, player):
        """
        Search for win condition in all configurations.
        Args:
            player (str): Current player

        """
        self.check_rows(player)
        self.check_columns(player)
        self.check_diagonal(player)

    @skip_if_winner_exist(mutex)
    def check_rows(self, player):
        """
        Check rows for win condition.
        Args:
            player (str): Current player

        """
        for row in self.board:
            self.search_for_winner(row, player)

    @skip_if_winner_exist(mutex)
    def check_columns(self, player):
        """
        Check columns for win condition.
        Args:
            player (str): Current player

        """
        for column in self.board.T:
            self.search_for_winner(column, player)

    @skip_if_winner_exist(mutex)
    def check_diagonal(self, player):
        """
        Check diagonals for win condition.
        Args:
            player (str): Current player

        """
        diagonal = self.board.diagonal()
        self.search_for_winner(diagonal, player)
        antidiagonal = self.board[::-1].diagonal()
        self.search_for_winner(antidiagonal, player)

    def search_for_winner(self, entity, player):
        """
        Search for winner in single list.
        Args:
            entity (list): List with possible win condition
            player (str): Current player

        """
        if all(field == player for field in entity):
            self.mark_winner(player)

    def mark_winner(self, winner):
        """
        Set winner to object.
        Args:
            winner (str): The winner

        Returns:

        """
        mutex.acquire()
        self.winner = winner
        mutex.release()
        exit()

    def proclaim_winner(self):
        """
        Returns the final winner state.
        Returns:
            winner (str): The winner

        """
        if self.winner:
            return self.winner
        else:
            return '.'


if __name__ == "__main__":
    board = Board()
    ticTacToe = TicTacToe(board.get_board())
    print(ticTacToe.check_win())
