appdirs==1.4.3
ddt==1.1.1
numpy==1.12.1
packaging==16.8
pyparsing==2.2.0
six==1.10.0
