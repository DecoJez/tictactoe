# TicTacToe #
As numpy library is allowed the iteration over matrix is quite easy.

Using threads, decorators and mutex for spicy up.

Tests are included for both Board and TicTacToe class.

### How do I set up? ###

* Create virtualenv (python3)
* Install dependencies from req.txt
* Run TicTacToe.py
* Type each row of matrix and confirm by Enter
* After last row press Enter again
* Enjoy